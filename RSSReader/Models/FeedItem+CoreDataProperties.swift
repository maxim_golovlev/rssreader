//
//  FeedItem+CoreDataProperties.swift
//  RSSReader
//
//  Created by Admin on 11.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import MagicalRecord

extension FeedItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FeedItem> {
        return NSFetchRequest<FeedItem>(entityName: "FeedItem");
    }

    @NSManaged public var title: String?
    @NSManaged public var link: String?
    @NSManaged public var contentSnippet: String?
    @NSManaged public var feedItemID: String?
    
    class func deleteElement(_ element: NSManagedObject, completion: @escaping () -> ()) {
        
        MagicalRecord.save({ localContext in
            element.mr_deleteEntity(in: localContext)
            
        },completion: { _ in completion() })
    }
    
    class func addElement(_ element: NSManagedObject, completion: @escaping () -> ()) {
        
        MagicalRecord.save({ localContext in
            FeedItem.mr_import(from: element, in: localContext)
            
        },completion: { _ in completion() })
    }
}
