//
//  SearchViewController.swift
//  RSSReader
//
//  Created by Admin on 11.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit
import MagicalRecord

class SearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBtm: NSLayoutConstraint!
    var entries = [FeedItem]()
    var feedViewController: FeedsViewController {
        return (self.parent as! FeedsViewController)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        searchBar.placeholder = "Search for RSS Feeds"
        addNotifications()
    }
    
    deinit {
        removeNotifications()
    }
    
    func addNotifications() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil, queue: OperationQueue.main) {
            [weak self] notification in
            self?.handleKeyboardAppearance(notification)
        }
    }
    
    func removeNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func handleKeyboardAppearance(_ notification: Notification) {
        
        var keyboardHeight = CGFloat(0)
        
        if let frame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if frame.origin.y < UIScreen.main.bounds.height {
                keyboardHeight = min(CGFloat(frame.size.width), CGFloat(frame.size.height))
            } else {
                keyboardHeight = 0
            }
        }
        tableViewBtm.constant = keyboardHeight
    }
    
    func performSearchForText(_ text: String) {
        print("Performing search for \(text), please wait...")
        
        let urlString = "https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&q=\(text)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let url = URL(string: urlString)
        
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            if error != nil {
                EZAlertController.alert(error!.localizedDescription)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? [String:AnyObject]
                let responseData = json?["responseData"] as? [String:AnyObject]
                if let entryDictionaries = responseData?["entries"] as? [[String:AnyObject]] {
                    self.entries = [FeedItem]()
                    for entryDictionary in entryDictionaries {
                        self.entries.append(FeedItem.mr_import(from: entryDictionary))
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView?.reloadData()
                })
                
            } catch let error {
                EZAlertController.alert(error.localizedDescription)
            }
        }.resume()
    }
    
    class func present(in container: UIView, of parent: UIViewController) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "SearchViewController")
        parent.addAsChildViewController(vc, toView: container)
        vc.view.constrainViewEqual(container)
        container.constraints.last!.constant = container.bounds.height
        vc.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: .curveEaseInOut, animations: {
            container.isHidden = false
            container.constraints.last!.constant = 0
            parent.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    class func dismiss(in container: UIView, of parent: UIViewController) {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: .curveEaseInOut, animations: {
            container.constraints.last!.constant = container.bounds.height
            parent.view.layoutIfNeeded()
        }, completion: { _ in
            parent.removeChildVC()
            container.isHidden = true
        })
    }
}

extension SearchViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text { performSearchForText(text) }
    }
}

extension SearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RssTableViewCell", for: indexPath) as! RssTableViewCell
        cell.rssItem = entries[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Results"
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {        return UITableViewAutomaticDimension
    }
}

extension SearchViewController: UITableViewDelegate {
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let feedItem = entries[indexPath.row]
        FeedItem.addElement(feedItem) { [unowned self] in
            self.feedViewController.rssTableView.reloadData()
            self.feedViewController.rightBarButtonItem.title = "Add"
            SearchViewController.dismiss(in: self.view.superview!, of: self.parent!)
        }
     }
}
