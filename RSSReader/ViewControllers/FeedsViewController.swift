//
//  ParserTableViewController.swift
//  RSSReader
//
//  Created by Admin on 09.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class FeedsViewController: UIViewController {
    
    @IBOutlet weak var rssTableView: UITableView!
    @IBOutlet weak var searchFeedsContainer: UIView!
    @IBOutlet weak var searchContainerTop: NSLayoutConstraint!
    @IBOutlet weak var rightBarButtonItem: UIBarButtonItem!
    var entries: [FeedItem] {
        return FeedItem.mr_findAll() as! [FeedItem]
    }

    override func viewDidLoad() {
        super.viewDidLoad()        
       
        rssTableView.tableFooterView = UIView()
    }
    
    @IBAction func insertNewRssItem(_ sender: Any) {
        
        if searchFeedsContainer.subviews.count > 0 {
            SearchViewController.dismiss(in: searchFeedsContainer, of: self)
            rightBarButtonItem.title = "Add"
        } else {
            SearchViewController.present(in: searchFeedsContainer, of: self)
            rightBarButtonItem.title = "Cancel"
        }
    }
}

extension FeedsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RssTableViewCell", for: indexPath) as! RssTableViewCell
        cell.rssItem = entries[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Saved"
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {        return UITableViewAutomaticDimension
    }
}

extension FeedsViewController: UITableViewDelegate {
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let feedItem = entries[indexPath.row]
        if let url = URL(string: feedItem.link ?? "") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let element = entries[indexPath.row]
            FeedItem.deleteElement(element) {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }            
        }
    }
}
