//
//  RssTableViewCell.swift
//  RSSReader
//
//  Created by Admin on 09.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class RssTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var rssItem: FeedItem? {
        didSet {
            guard let data = rssItem?.title?.data(using: String.Encoding.unicode) else { return }
            let options = [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType]
            do {
                let htmlText = try(NSAttributedString(data: data, options: options, documentAttributes: nil))
                self.titleLabel.attributedText = htmlText
            } catch let error {
                EZAlertController.alert(error.localizedDescription)
            }
        }
    }
}
