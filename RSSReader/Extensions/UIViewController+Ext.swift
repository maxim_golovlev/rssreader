//
//  UIViewController+Ext.swift
//  RSSReader
//
//  Created by Admin on 11.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func addAsChildViewController(_ vc: UIViewController, toView: UIView) {
        self.addChildViewController(vc)
        toView.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
    }
    
    func removeChildVC() {
        guard let childController = childViewControllers.last else { return }
        childController.willMove(toParentViewController: nil)
        childController.view.removeFromSuperview()
        childController.didMove(toParentViewController: nil)
        childController.removeFromParentViewController()
    }
}
