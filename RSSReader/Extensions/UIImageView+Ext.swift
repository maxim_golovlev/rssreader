//
//  UIImageView+Ext.swift
//  RSSReader
//
//  Created by Admin on 09.12.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    public func imageWithUrl(url: String) {
        requestImage(url, success: { (image) -> Void in
            if let img = image {
                self.image = img
            }
        })
    }
    
    func requestImage(_ url: String, success: @escaping (UIImage?) -> Void) {
        requestURL(url, success: { (data) -> Void in
            if let d = data {
                success(UIImage(data: d))
            }
        })
    }
    
    fileprivate func requestURL(_ url: String, success: @escaping (Data?) -> Void, error: ((Error) -> Void)? = nil) {
        guard let requestURL = URL(string: url) else {
            assertionFailure("Error: Invalid URL")
            return
        }
        
        URLSession.shared.dataTask(
            with: URLRequest(url: requestURL),
            completionHandler: { data, response, err in
                if let e = err {
                    error?(e as NSError)
                } else {
                    success(data)
                }
        }).resume()
    }
}
